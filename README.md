# password-management

## Getting started
* Install a KeePass client. We reccomend [KeePassXC](https://keepassxc.org/)
* Open `Database.kdbx`
* Enter master password
* Read/edit shared passwords
* Commit any changes and submit a PR
